require "time"

def measure( n = 1 , &block )
  number = n
  start_time = Time.now()
  while n > 0
   block.call
   n -=1
  end
  end_time = Time.now()
(end_time - start_time)/number 

end
