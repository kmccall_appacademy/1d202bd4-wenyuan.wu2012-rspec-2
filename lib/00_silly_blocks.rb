def reverser
  arr = yield.split(" ")
  new_str = ""
  arr.each { |word| new_str += "#{word.reverse} " }
  new_str[0..-2]
end

def adder(num = 1, &block)
  block.call + num
end

def repeater(num = 1, &block)
  while num > 0
    block.call
    num -= 1
  end
end
